package com.gitee.little.miser.dto;

import com.gitee.little.miser.annotation.EnumCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TestEnum2 {

    ONE1(1,"一"),
    TWO2(2,"二"),
    ;

    @EnumCode
    private Integer code;

    private String value;
}
