package com.gitee.little.miser.dto;

import lombok.Data;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

/**
 * @Author: wanxuefeng
 * @Description:
 * @CreateTime: 2023-10-10  17:40
 */

@Data
public class UserEditDto {

    private String name;

    private LocalDateTime creatTime;

    private String updateTime;
}
