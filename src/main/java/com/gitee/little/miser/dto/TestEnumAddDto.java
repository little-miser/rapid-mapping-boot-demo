package com.gitee.little.miser.dto;

import com.gitee.little.miser.annotation.RapidMapping;
import lombok.Data;

/**
 * @Author:zyq
 * @Description: 测试枚举
 * @CreateTime: 2024-04-27  17:39
 */
@Data
public class TestEnumAddDto {

    private TestEnum count;

    private String name;
}
