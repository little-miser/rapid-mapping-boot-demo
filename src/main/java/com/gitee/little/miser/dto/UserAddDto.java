package com.gitee.little.miser.dto;

import lombok.Data;
import java.util.Date;

/**
*@Author: wanxuefeng
*@Description:
*@CreateTime: 2023-10-10  17:40
*/
@Data
public class UserAddDto {

    private String userName;

    private Date creatTime;

    private String updateTime;
}
