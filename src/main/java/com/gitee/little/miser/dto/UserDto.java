package com.gitee.little.miser.dto;

import com.gitee.little.miser.annotation.RapidMapping;
import com.gitee.little.miser.annotation.RapidMappingField;
import com.gitee.little.miser.utils.DateUtils;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: wanxuefeng
 * @Description:
 * @CreateTime: 2023-10-10  17:38
 */
@RapidMapping({UserAddDto.class, UserEditDto.class, UserDto.class})
@Data
public class UserDto {

    @RapidMappingField(value = "name",targetClass = UserEditDto.class)
    private String userName;

    @RapidMappingField(dateFormat = DateUtils.FORMAT_SECOND)
    private String creatTime;

    @RapidMappingField(targetClass = UserEditDto.class, dateFormat = DateUtils.FORMAT_MILLISECOND)
    @RapidMappingField(dateFormat = DateUtils.FORMAT_NO_MILLISECOND)
    private LocalDateTime updateTime;
}
