package com.gitee.little.miser;

import com.gitee.little.miser.dto.*;
import com.gitee.little.miser.spring.annotation.EnableRapidMapping;
import com.gitee.little.miser.utils.MappingUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.time.LocalDateTime;

@EnableRapidMapping
@SpringBootApplication
public class RapidMappingDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RapidMappingDemoApplication.class, args);

        TestEnumAddDto testEnumAddDto = new TestEnumAddDto();
        testEnumAddDto.setName("123");
        testEnumAddDto.setCount(TestEnum.ONE);

        TestEnumDto convert = MappingUtil.convert(TestEnumDto.class, testEnumAddDto);
        System.out.println(convert);
        UserDto userDto = new UserDto();
        userDto.setUserName("张三");
        userDto.setUpdateTime(LocalDateTime.now());
        userDto.setCreatTime("2023-10-10 10:16:56");

        UserAddDto userAddDto = MappingUtil.convert(UserAddDto.class, userDto);
        System.out.println(userAddDto);
        UserEditDto userEditDto = MappingUtil.convert(UserEditDto.class, userDto);
        System.out.println(userEditDto);

    }

}
